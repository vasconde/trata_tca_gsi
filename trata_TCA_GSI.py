
# trata_TCA_GSI.py
# Programa para tratar os ficheiros GSI vindos do taqueometro eletronico TCA
# Vasco Conde 2013


# interface grafica com o utilizador onde sao pedidos
# os caminhos para os ficheiros
def file_names ():

    import tkinter
    from tkinter.filedialog import askopenfilename
    from tkinter.filedialog import asksaveasfilename

    root = tkinter.Tk()
    # show askopenfilename dialog without the Tkinter window
    root.withdraw()

    all_files = True

    print('Ficheiro GSI')
    file_name1 = askopenfilename(title='Ficheiro GSI')
    print(file_name1)

    print('Ficheiro Observacoes')
    file_name2 = asksaveasfilename(title='Ficheiro observacoes')
    print(file_name2)

    print('Ficheiro Atmosfera')
    file_name3 = asksaveasfilename(title='Ficheiro atmosfera')
    print(file_name3)

    return [file_name1, file_name2, file_name3]

# o ficheiro GSI e passado linha por linha para uma lista
def GSI2list (file_name):
    
    # read line by line and put it in a list
    gsi_file = open(file_name, 'rt')

    lines = [line.strip() for line in gsi_file] #list of strings

    gsi_file.close()

    return lines

def agrupaGiros (lines):

    # separa por .GSI
    
    grupos = []
    for line in lines:

        if '.gsi' in line.lower():
            grupos.append([])

        grupos[-1].append(line)

    # retira aqueles que so tem o nome
    # ou so o nome e os parametros atmosfericos
    grupos2 = []
    for grupo in grupos:
        
        n = len(grupo)
        
        n_temp = 0

        for elemento in grupo:
            if 'Temp. :' in elemento:
                n_temp+=1
        
        # numero de elementos menos o numero de linhas
        # relativos ah atmosfera menos o numero de linhas com .GSI (que e so 1)
        if n - n_temp - 1 != 0:
            grupos2.append(grupo)
        
#    print(len(grupos))
#    print(len(grupos2))

    return grupos2

"""
    grupos2 = []
    for grupo in grupos:
        if len(grupo) > 2:
            grupos2.append(grupo)

    for grupo in grupos2:
        print (grupo)
        print()
        print(10 * '-')
        print()
"""

class Obs:
    def __init__(self, s):
        self.nome = ''
        self.az = 0
        self.z = 0
        self.d = 0

        self.str2obs (s)

    def str2obs (self,s):
        # tira os espacos da string
        self.nome = ''.join(s[7:16].split())

        self.az = float(s[23:31])
        self.z = float(s[39:47])
        self.d = float(s[55:63])

class Atmo:
    def __init__(self, s):
        self.t = 0
        self.p = 0
        self.h = 0

        self.str2atmo (s)

    def str2atmo (self,s):
        self.t = float( s[s.index('Temp. : ')+len('Temp. : '):s.index('C')] )
        self.p = float( s[s.index('Pres. : ')+len('Pres. : '):s.index(' mBar')] )
        self.h = float( s[s.index('Hum. : ')+len('Hum. : '):s.index('%')] )

class Giro:

    def __init__(self, name, l):

        self.name = name
        
        self.atmos = []
        self.obss = []

        self.list2giro (l)

    def list2giro (self,l):
        
        for elemento in l[1:]:
            if not('Temp. :' in elemento):
                self.obss.append( Obs(elemento) )
            else:
                self.atmos.append( Atmo(elemento) )
        
class Giros:

    def __init__(self):
        self.giros = []

    def addGiro(self, giro):
        self.giros.append(giro)

    def removeGiro(self, giro):
        self.giros.remove(giro)

    # funcao para escrever as observacoes nos respetivos ficheiros
# eh designado de puro por nao efetuar calculos, apenas retira
# o lixo do GSI
    def escreve_txt_puro(self, giros_file_path, atmo_file_path):
    
        # observacoes geodesicas
        giros_file = open(giros_file_path, 'wt')

        for g in self.giros:
            giros_file.write('\n')
            giros_file.write('{0}\n'.format(g.name))
            giros_file.write('\n')
            for o in g.obss:
                giros_file.write('{0:10} {1:15.1f} {2:15.1f} {3:15.1f}\n'.format(o.nome, o.az, o.z, o.d))

        giros_file.close()

        # observacoes de atmosfera
        atmo_file = open(atmo_file_path, 'wt')
        for g in self.giros:
            atmo_file.write('\n')
            atmo_file.write('{0}\n'.format(g.name))
            atmo_file.write('\n')
            for a in g.atmos:
                atmo_file.write('{0:15.1f} {1:15.1f} {2:15.1f}\n'.format(a.t, a.p, a.h))
        atmo_file.close()


########################### MAIN ##################################

# recolhe o nome dos ficheiros de entrada e saida
#files_names = file_names ()
files_names = ["//home//vasconde//Documentos//devel//trata_tca_gsi//data_ac//FILE10V.GSI",
               "//home//vasconde//Documentos//devel//trata_tca_gsi//data_ac//obs.txt",
               "//home//vasconde//Documentos//devel//trata_tca_gsi//data_ac//atmo.txt"]

# estrai uma lista das linhas dos ficheiros
lines = GSI2list (files_names[0])

# agrupa os giros. ou seja uma lista de listas
# aqui e feita uma limpeza em que sao retirados aqueles grupos
# para os quais so se tem o nome e/ou os parametros atmosfericos
grupos = agrupaGiros (lines)

#listaGiros = []
#for grupo in grupos:
#    g = Giro(grupo[0], grupo)
#    
#    listaGiros.append( g )

# preenche o objeto com os giros
campanha = Giros()
for grupo in grupos:
    g = Giro(grupo[0], grupo)
    campanha.addGiro(g)

# Escreve na consola os dados em bruto


## Apresentacao preliminar
## SUGESTAO: passar isto para uma funcao

i = 0
for g in campanha.giros:
    print()
    print('({0}) {1}'.format(i, g.name))
    print()
    for o in g.obss:
        print ('{0:10} {1:15.1f} {2:15.1f} {3:15.1f}'.format(o.nome, o.az, o.z, o.d))
    i = i + 1


print('Eliminar: ')
eliminar = input()
if eliminar != '':
    elementos = []
    for e in eliminar.split(): # parte a string nos indices
        elementos.append(campanha.giros[int(e)]) # preenche 'elementos' com os giros a eliminar
    for e in elementos:
        campanha.removeGiro(e) # remove os giros a eleminar

# volta a msotrar os giros depois da remocao
i = 0
for g in campanha.giros:
    print()
    print('({0}) {1}'.format(i, g.name))
    print()
    for o in g.obss:
        print ('{0:10} {1:15.1f} {2:15.1f} {3:15.1f}'.format(o.nome, o.az, o.z, o.d))
    i = i + 1


# Imprime o resultado em ficheiros de texto
campanha.escreve_txt_puro(files_names[1],files_names[2])
